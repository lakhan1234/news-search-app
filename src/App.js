import React from 'react';
import logo from './logo.svg';
import './App.css';
import News from './containers/News/News';
import { Route, Switch } from 'react-router-dom';
import { withRouter } from 'react-router-dom';
import NewsDetail from './components/NewsDetail/NewsDetail';
function App() {
  return (
    <div>
      <Switch>
          <Route path="/" exact component={News} />
          <Route path="/news/:id" exact component={NewsDetail} />
      </Switch>
      
    </div>
  );
}

export default withRouter(App);
