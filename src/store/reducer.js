import * as actionTypes from './action-types';

const initialState = {
    articles: [],
    article: []
    
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_ARTICLES:
            return {
                ...state,
                articles: action.articles
            }
        case actionTypes.GET_ARTICLE_DETAIL:
            return {
                ...state,
                article: state.articles[action.id]
            }
        default:
            return state;
    }

};

export default reducer;