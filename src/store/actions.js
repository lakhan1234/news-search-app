import {
    GET_ARTICLES,
    GET_ARTICLE_DETAIL
    } from './action-types.js'


import axios from '../axios-config';
//add cart action
const API_URL = 'http://www.mocky.io/v2/5d8686a032000024b607b40e';

export const getArticles = () => dispatch => {
    return new Promise((resolve, reject) => {
        axios.get(API_URL)
            .then(response => {
                
                dispatch({
                    type: GET_ARTICLES,
                    articles: response.data.articles
                });
                resolve('success');
            })
            .catch((error) => {
                console.log(error);
                reject(error);
            })
    })
}

export const getArticleDetail = (id) => {
    return {
        type: GET_ARTICLE_DETAIL,
        id
    }
}
