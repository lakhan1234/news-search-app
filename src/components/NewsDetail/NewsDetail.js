import React, { useEffect } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import { getArticleDetail } from '../../store/actions';
import { withRouter } from 'react-router-dom';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

const NewsDetail = (props) => {

    useEffect(() => {
        if (props.match.params.id) {
            props.getArticleDetail(props.match.params.id);
        }
        
    }, []);

    const backToList = () => {
        props.history.replace('/');
    }
    let articlesList = null;
    let siteName = '';
    if(props.article !== undefined) {
        if(props.article.source !== undefined) {
        siteName = props.article.source.name;
        }
        articlesList = <Grid container mt={50} pt={50}>
            <Button size="small" color="primary" onClick={()=>backToList()}>
                Back
            </Button>
            <Typography gutterBottom variant="h6" component="h2">
                {props.article.title}
            </Typography>
        
        <Card>
      <CardActionArea>
        
        <CardContent>
            <Typography variant="caption" display="block" gutterBottom>
                {props.article.publishedAt}
            </Typography>
            
            <Typography variant="caption" display="block" gutterBottom>
                <Box fontStyle="italic">
                    {props.article.author}
                </Box>
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
                {props.article.description}
            </Typography>
        </CardContent>
        <CardMedia
          component="img"
          alt="Article Picture"
          height="180"
          image={props.article.urlToImage}
          title="Contemplative Reptile"
        />
      </CardActionArea>
      <CardActions>
        <Button size="small" color="primary">
          {siteName}
        </Button>
        
      </CardActions>
    </Card>
    </Grid>
    }


    return (
        <React.Fragment>
            <CssBaseline />
            <Container maxWidth="lg" pt={50} mt={50} m={50} style={{marginTop:'50px'}}>
                {articlesList}
            </Container>
        </React.Fragment>
    )
}


const mapStateToProps = state => {
    console.log('article', state.article);
    return {
        article: state.article
    };
};



export default connect(mapStateToProps, { getArticleDetail })(withRouter(NewsDetail));

