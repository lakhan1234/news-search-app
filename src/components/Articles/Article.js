import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
const useStyles = makeStyles({
  card: {
  },
});

export default function Article(props) {
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <CardActionArea onClick={props.clicked}>
        <CardMedia
          component="img"
          alt="Article Picture"
          height="180"
          image={props.urlToImage}
          title="Contemplative Reptile"
        />
        <CardContent>
            <Typography variant="caption" display="block" gutterBottom>
                {props.publishedAt}
            </Typography>
            <Typography gutterBottom variant="h6" component="h2">
                {props.title}
            </Typography>
            <Typography variant="caption" display="block" gutterBottom>
                <Box fontStyle="italic">
                    {props.author}
                </Box>
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
                {props.description}
            </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button size="small" color="primary">
          {props.siteName}
        </Button>
        
      </CardActions>
    </Card>
  );
}