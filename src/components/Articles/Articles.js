import React, { useEffect, useDispatch } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Article from './Article';
import { connect } from 'react-redux';
import { getArticles } from '../../store/actions';
import { withRouter } from 'react-router-dom';
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));


function Articles(props) {

    const showNewsDetail = (index) => {
        const queryParam = [];
        props.history.push({
            pathname: '/news/' + index,
            search: '?' + queryParam
        });
    }

    const classes = useStyles();
    useEffect(() => {
        props.getArticles();
    }, []);

    let articlesList = null;
    if(props.articles.length) {
        articlesList = props.articles.map((article, index) => (
            <Grid item xs={6} sm={4} md={3} lg={3} key={'article_header'+index}>
            <Article 
                key={'article'+index}
                author = {article.author}
                description = {article.description}
                title = {article.title}
                urlToImage = {article.urlToImage}
                url = {article.url}
                publishedAt = {article.publishedAt}
                siteName = {article.source.name}
                clicked = {()=>showNewsDetail(index)}
            />
            </Grid>
        ));
    }
    return (
        <Grid container spacing={3}>
            {articlesList}
        </Grid>
    );
}


const mapStateToProps = state => {
    console.log(state.articles);
    return {
        articles: state.articles
    };
};



export default connect(mapStateToProps, { getArticles })(withRouter(Articles));



