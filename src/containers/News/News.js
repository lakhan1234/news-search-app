import React , { Component } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import NewsSearch from '../../components/NewsSearch/NewsSearch';
import Articles from '../../components/Articles/Articles';
class News extends Component {

    render() {
        return (
            <React.Fragment>
                <CssBaseline />
                <Container maxWidth="lg" pt={50}>
                    {/* <Typography component="div" style={{ backgroundColor: '#ffffff', height: '100vh', paddingTop:'50px' }}> */}
                        <NewsSearch />
                        <Articles />
                    {/* </Typography> */}
                </Container>
            </React.Fragment>
        )
    }
}

export default News;